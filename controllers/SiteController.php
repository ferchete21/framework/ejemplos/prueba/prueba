<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Productos;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;




class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionCategorias(){
        //ListView
           
            $dataProvider = new ActiveDataProvider([
            "query" => Productos::find() -> select ('categorias')->distinct(),
        ]);
            return $this->render('categorias',[
                
            'dataProvider' => $dataProvider
        ]);        
    }
    
    public function actionProductos(){
        //GridView - tabla
           
            $dataProvider = new ActiveDataProvider([
            "query" => Productos::find(),
        ]);
            return $this->render('productos',[
                
            'dataProvider' => $dataProvider
        ]);        
    }
    
    public function actionOfertas(){
        //ListView - lista
           
            $dataProvider = new ActiveDataProvider([
            "query" => Productos::find() -> where (['oferta'=>1]),
        ]);
            return $this->render('ofertas',[
                
            'dataProvider' => $dataProvider
        ]);        
    }
    
    public function actionVer($categoria){
        $productos = Productos::find()->where(['categorias'=>$categoria]);
        $dataProvider = new ActiveDataProvider([
            'query' => $productos,
            ]);
            return $this->render('ver',[
                'dataProvider'=> $dataProvider,
                'categoria' => $categoria
            ]);
        
    }
    
    public function actionDonde() {
        return $this-> render('donde');
    }
    
    public function actionQuienes() {
        return $this-> render('quienes');
    }
    
    public function actionNuestros(){
        $fotos= Productos::find()
                ->select("foto")
                ->where(["oferta" => 1])
                ->asArray()
                ->all();
        $fotos= ArrayHelper::getColumn($fotos, "foto");
        $salida=[];
        foreach ($fotos as $src){
            $salida[]=Html::img("@web/imgs/$src");
        }
        return $this-> render('nuestros',[
            "fotos" =>$salida,
        ]);
    }
    
    public function actionInformacion() {
        return $this-> render('informacion');
    }

    
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
    public function actionVermas($id){
        //metodo1
       // $salida=Productos::find()->where(["id"=>$id]);
        //metodo2
        $salida= Productos::findOne($id);
        
        return $this->render("vermas",[
            "model" => $salida
        ]);
        
        
    }

}
