-- -- phpMyAdmin SQL Dump
-- -- version 5.1.0
-- -- https://www.phpmyadmin.net/
-- --
-- -- Servidor: 127.0.0.1
-- -- Tiempo de generación: 11-10-2021 a las 10:01:00
-- -- Versión del servidor: 10.4.19-MariaDB
-- -- Versión de PHP: 8.0.6
-- 
-- SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
-- START TRANSACTION;
-- SET time_zone = "+00:00";
-- 
-- 
-- /*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
-- /*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
-- /*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
-- /*!40101 SET NAMES utf8mb4 */;
-- 
-- --
-- -- Base de datos: `prueba1`
-- --
-- CREATE DATABASE IF NOT EXISTS `prueba1` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
-- USE `prueba1`;
-- 
-- -- --------------------------------------------------------
-- 
-- --
-- -- Estructura de tabla para la tabla `productos`
-- --
-- 
-- DROP TABLE IF EXISTS `productos`;
-- CREATE TABLE `productos` (
--   `id` int(11) NOT NULL,
--   `nombre` varchar(100) NOT NULL,
--   `foto` varchar(100) NOT NULL,
--   `descripcion` varchar(800) NOT NULL,
--   `precio` float DEFAULT NULL,
--   `oferta` tinyint(1) DEFAULT 0,
--   `categorias` varchar(200) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
-- 
-- --
-- -- Índices para tablas volcadas
-- --
-- 
-- --
-- -- Indices de la tabla `productos`
-- --
-- ALTER TABLE `productos`
--   ADD PRIMARY KEY (`id`);
-- COMMIT;
-- 
-- /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
-- /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
-- /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-10-2021 a las 10:43:59
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `productosjota`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `descripcion` varchar(2000) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `oferta` tinyint(1) DEFAULT NULL,
  `categorias` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `foto`, `descripcion`, `precio`, `oferta`, `categorias`) VALUES
(1, 'Espejo rectangular Shadi Taco roble 178 x 78 cm', '001.png', 'Espejo decorativo de moldura de madera con acabado en color roble. Su diseño escandinavo adornará tus estancias favoritas a la vez que agrandará visualmente pasillos, estancias y habitaciones. Al ser un elemento de grandes dimensiones es idóneo para vestidores, habitaciones principales o como recibidor en la entrada de tu casa, muy decorativo tanto colgado como apoyado en el suelo. La madera cuenta con certificado PEFC que asegura su procedencia de bosques gestionados de manera sostenible y respetuosa con el equilibrio social, económico y medioambiental. Medida total exterior: 78 x 178 cm (ancho x alto).', 98.99, 0, 'decoracion'),
(2, 'Taladro percutor BOSCH BLUE 18V 2baterías 3AH', '002.png', 'Taladro percutor sin cable Bosch GSB 18V-21 Professional de 18 V de uso profesional es intenso ideal para perforar y atornillar incluso en el hormigón, con motor de carbón potente y fiable. Gracias a su batería de ion-litio (2 baterías y cargador incluidos), con una intensidad de 3 Ah y un tiempo de carga de 35 minutos, podrás realizar trabajos en altura y en exterior con total autonomía y libertad, sin enchufes ni alargadores. Además, su indicador de nivel de carga permite anticiparte y cargar la batería antes de que un trabajo pueda quedar inacabado. El diámetro de perforación en la madera es de 35 mm, en el metal es de 10 mm y en el hormigón es de 10 mm, con una fuerza de apriete de 55 Nm y un par de apriete de 55/21 Nm, alcanzando una velocidad de 1.800 rpm. Su función percutora permite obtener resultados óptimos con el mínimo esfuerzo. Incorpora una luz integrada para trabajos con escasa iluminación. El mandril es de autoapriete y tiene un diámetro de entre 1,5 y 13 mm. Dispone de un sistema de control de velocidad variable que permite adaptar la velocidad en función del material, con 2 velocidades. Su empuñadura ergonómica permite un uso más cómodo de la herramienta. Este modelo incluye 2 baterías de 3 Ah, 1 cargador GAL 18V-20 y 1 set de 25 puntas L-CASE. Cuenta con una garantía de 2 años. Peso: 1,5 kg. Medidas: 23 x 20,9 cm (alto x fondo). ', 179, 1, 'herramientas'),
(3, 'Taladro con cable BLACK+DECKER 710 W + maletín y accesorios', '003.png', 'Taladro percutor con cable Black+Decker para atornillar y taladrar madera, metal, placa de cartón-yeso y materiales más duros de manera más fácil. Su diseño compacto y manejable está equipado con un potente motor de 710 W, indicado para uso doméstico y ocasional. Consigue taladrar en hormigón y bloque con la función percutora con un máximo de 47.600 golpes por minuto, perforando hasta 13 mm de diámetro en hormigón. Además, con los accesorios adecuados podrás realizar agujeros para enchufes, focos y tubos, y eliminar barnices y pinturas lijando las superficies. Para una sujeción más cómoda y segura, el taladro dispone de una empuñadura ergonómica y suave que facilita su uso. Gracias a su bloqueo de gatillo y mango lateral robusto con tope de profundidad, se reduce el esfuerzo durante los trabajos ininterrumpidos. La herramienta permite variar la velocidad hasta un máximo de 2.800 r.p.m. consiguiendo un mayor control y adaptación a las características y dureza de cada material. El cambio de broca es fácil y rápido gracias a su portabrocas automático sin necesidad de llave. Incluye set de 4 brocas (2 brocas para madera 4/6 mm y 2 brocas para mampostería 4/6 mm) y maletín para guardar la herramienta cómodamente.', 48.99, 1, 'herramientas'),
(4, 'Set de porche de aluminio y madera NATERIAL Fima 4 personas', '004.png', 'Set de muebles para jardín o terraza modelo Fima compuesto por 1 mesa, 1 sofá y 2 sillones con capacidad para reunir a 4 personas en el exterior de tu vivienda. Está fabricado en estructura de aluminio y madera de acacia con certificación FSC (certifica que la madera procede bosques gestionado de manera sostenible y respetuosa con el medioambiente. Incluye cojines de poliéster desenfundables, que facilita su limpieza. El producto ya está montado. Para su mantenimiento, se recomienda ubicarlo bajo cubierta, protegido del sol directo, y guardarlo en un lugar seco y ventilado cuando no lo uses (o cubrir con una funda para muebles). Medidas de la mesa: 110 x 42 x 52 cm. Sofá: 144 x 62 x 74 cm. Sillones: 70 x 62 x 74 cm (ancho x alto x fondo). ', 619.65, 1, 'jardin');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;