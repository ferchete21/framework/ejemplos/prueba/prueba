<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $id
 * @property string $nombre
 * @property string $foto
 * @property string $descripcion
 * @property float|null $precio
 * @property int|null $oferta
 * @property string $categorias
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nombre', 'foto', 'descripcion', 'categorias'], 'required'],
            [['id', 'oferta'], 'integer'],
            [['precio'], 'number'],
            [['nombre', 'foto'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 2000],
            [['categorias'], 'string', 'max' => 200],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'foto' => 'Foto',
            'descripcion' => 'Descripcion',
            'precio' => 'Precio',
            'oferta' => 'Oferta',
            'categorias' => 'Categorias',
        ];
    }
}
