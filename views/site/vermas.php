<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',               // title attribute (in plain text)
        'description',    // description attribute in HTML
        'categoria',
        'precio',
        'oferta',
        [
            'atribute'=>'foto',
            'label'=>'foto',
            'format'=>'raw',
            'value' => function($data){
                $url = '@web/imgs/' . $data->foto;
                return Html::img($url,[
                    'class'=>'img-fluid',
                    'style'=> 'width:300px']); 
            }
        ],
       
    ],
]);