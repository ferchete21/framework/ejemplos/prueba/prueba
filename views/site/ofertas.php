<?php

use yii\widgets\ListView;
?>
<h1 class="list-group-item">Ofertas:</h1>
<?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView'=>'_ofertas',
    'options'=>[
        'class'=>'row'
        ],
    'layout'=>"{items}",
    "itemOptions" => [
        'class' => 'col-lg-5 ml-auto mr-auto bg-light p-3 mb-5',
    ],

]);
