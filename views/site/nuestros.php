<?php
use yii\bootstrap4\Carousel;
use yii\helpers\Html;
?>
<div align="center">
    <p>
        Mostraremos productos, cuando tengamos productos que mostrar
    </p>
    <?php
    echo Carousel::widget([
    'items' => $fotos,
        'options'=>[
            'class' => 'mx-auto col-lg-8'
        ],
        'controls' => ['<i class="fas fa-angle-double-left fa-5x"></i>','<i class="fas fa-angle-double-right fa-5x"></i>']
]);
    ?>

</div>