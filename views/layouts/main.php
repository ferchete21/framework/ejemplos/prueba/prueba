<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'encodeLabels'=>false,
        'items' => [
            ['label' => '<i class="fab fa-accessible-icon"></i> Inicio', 'url' => ['/site/index']],
            ['label' => '<i class="fas fa-biohazard"></i> Ofertas', 'url' => ['/site/ofertas']],
            ['label' => '<i class="fab fa-creative-commons-nc-eu"></i> Productos', 'url' => ['/site/productos']],
            ['label' => '<i class="fab fa-chromecast"></i> Categorías', 'url' => ['/site/categorias']],
            ['label' => '<i class="fas fa-capsules"></i> Nosotros', 'items' => [
            ['label' => '<i class="fas fa-cat"></i> Donde Estamos', 'url' => ['/site/donde']],
            ['label' => '<i class="fas fa-cloud-download-alt"></i> Quienes Somos', 'url' => ['/site/quienes']], 
            ['label' => '<i class="fas fa-dragon"></i> Nuestros Productos', 'url' => ['/site/nuestros']],
            '<div class="dropdown-divider"></div>',
                ['label' => 'Información', 'url' => ['/site/informacion']]]],            
            ['label' => '<i class="fas fa-fighter-jet"></i> Contacto', 'url' => ['/site/contact']],
            
//            Yii::$app->user->isGuest ? (
//                ['label' => 'Login', 'url' => ['/site/login']]
//            ) : (
//                '<li>'
//                . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
//                . Html::submitButton(
//                    'Logout (' . Yii::$app->user->identity->username . ')',
//                    ['class' => 'btn btn-link logout']
//                )
//                . Html::endForm()
//                . '</li>'
//            )
        ],
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <!--<p class="pull-left">Fernando Martinez - Alpe Formación 2017</p>-->
        <p class="float-right">Fernando Martinez, haciendo la prueba, a ver que sale</p>

    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
